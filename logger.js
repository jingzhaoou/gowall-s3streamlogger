var config = require('./config'),
    S3StreamLogger = require('./s3-streamlogger').S3StreamLogger;

var aws = config.aws,
    s3stream = new S3StreamLogger({
        bucket: aws.bucket,
        access_key_id: aws.access_key_id,
        secret_access_key: aws.secret_access_key,
    });

setInterval(function() {
    var d = { 'ts': new Date() };

    s3stream.write(JSON.stringify(d) + "\0");
}, 10 * 1000);
